package com.example.alunos.listadeimagens;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    Context context;

    Integer[] imagens = {R.drawable.android, R.drawable.android, R.drawable.android, R.drawable.android,
            R.drawable.android, R.drawable.android, R.drawable.android, R.drawable.android, R.drawable.android,
            R.drawable.android, R.drawable.android, R.drawable.android, R.drawable.android, R.drawable.android};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        listView= (ListView)findViewById(R.id.list_image);
        listView.setAdapter((ListAdapter)new CustomAdapter(this, imagens));
    }
}
