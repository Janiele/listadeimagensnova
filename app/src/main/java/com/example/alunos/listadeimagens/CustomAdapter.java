package com.example.alunos.listadeimagens;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Created by Alunos on 28/03/2018.
 */

public class CustomAdapter extends BaseAdapter{

    Integer[] img;
    Context context;

    private static LayoutInflater inflater = null; //é um metodo inflater

    public CustomAdapter(MainActivity mainActivity, Integer[] imagens ) {

        context = mainActivity;
        img = imagens;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() { //retornar a quantidade de imags q tem dentro do array
        return img.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    public class Holder{//
        ImageView im;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View roView1;

        roView1= inflater.inflate(R.layout.custom_layout, null);
        holder.im = (ImageView) roView1.findViewById(R.id.imageView);
        holder.im.setImageResource(img [i]);
        roView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You cliked" + img[i], Toast.LENGTH_SHORT).show();
            }
        });
        return roView1;
    }
}
